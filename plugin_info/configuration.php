<?php

/* This file is part of Jeedom.
*
* Jeedom is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Jeedom is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Jeedom. If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . '/../../../core/php/core.inc.php';
include_file('core', 'authentification', 'php');
if (!isConnect()) {
    include_file('desktop', '404', 'php');
    die();
}
?>


<form class="form-horizontal">
	<div class="form-group">
    <fieldset>
    <?php

    if (config::byKey('jeeNetwork::mode') == 'slave') {
		echo '<div class="alert alert-danger"><b>{{La configuration du plugin se fait uniquement sur le Jeedom principal}}</b></div>';
	} else {

	echo '<div class="form-group">';
  $nodeHost = config::byKey('nodeHost','mySensors');
  if (!$nodeHost || $nodeHost == "master" || $nodeHost == "network" || $nodeHost == "none" ) {
    $statusNode = mySensors::deamonRunning();
  } else {
    $jeeNetwork = jeeNetwork::byId($nodeHost);
    if (!$jeeNetwork) {
      $statusNode = false;
    } else {
      $jsonrpc = $jeeNetwork->getJsonRpc();
      if (!$jsonrpc->sendRequest('deamonRunning', array('plugin' => 'mySensors'))) {
        throw new Exception($jsonrpc->getError(), $jsonrpc->getErrorCode());
      }
      $statusNode = $jsonrpc->getResult();
    }
  }
  $statusGateway=false;
  $statusGateway = config::byKey('gateway','mySensors');
  $statusDep = mySensors::checkDep();

  echo '<label class="col-sm-4 control-label">{{Service}}</label>';
if (!$statusNode) {
	echo '<div class="col-sm-1"><span class="label label-danger tooltips" style="font-size : 1em;" title="{{Le service controlleur mySensors n\'est pas lancé}}">NOK</span></div>';
} else {
	echo '<div class="col-sm-1"><span class="label label-success" style="font-size : 1em;">OK</span></div>';
}
echo '<label class="col-sm-1 control-label">{{Gateway}}</label>';
if (!$statusGateway) {
	echo '<div class="col-sm-1"><span class="label label-danger tooltips" style="font-size : 1em;" title="{{Vérifiez la communication avec la gateway}}">NOK</span></div>';
} else {
	echo '<div class="col-sm-1"><span class="label label-success" style="font-size : 1em;">OK</span></div>';
}
echo '<label class="col-sm-1 control-label">{{Dépendances}}</label>';
if (!$statusDep) {
	echo '<div class="col-sm-1"><span class="label label-danger" style="font-size : 1em;">NOK</span></div>';
} else {
	echo '<div class="col-sm-1"><span class="label label-success" style="font-size : 1em;">OK</span></div>';
}
echo '</div>';


    echo '</div>';
    echo '<legend><i class="fa fa-cog"></i> {{Paramétrage}}</legend>';

if (jeedom::isCapable('sudo')) {
	echo '<div class="form-group">
				<label class="col-lg-4 control-label">{{Dépendances service Nodejs}}</label>
				<div class="col-lg-3">
					<a class="btn btn-warning bt_installDeps"><i class="fa fa-check"></i> {{Réinstaller les dépendances}}</a>
				</div>
			</div>';
} else {
	echo '<div class="alert alert danger">{{Jeedom n\'a pas les droits sudo sur votre système, il faut lui ajouter pour qu\'il puisse installer le démon openzwave, voir <a target="_blank" href="https://jeedom.fr/doc/documentation/installation/fr_FR/doc-installation.html#autre">ici</a> partie 1.7.4}}</div>';
}

    echo '<div id=globalSensors class="form-group">
            <label class="col-lg-4 control-label">{{Connexion à la Gateway : }}</label>
            <div class="col-lg-4">
				<select class="configKey form-control" id="select_mode" data-l1key="nodeHost">
					<option value="none">{{Aucun}}</option>
                    <option value="master">{{Jeedom maître}}</option>';

                    foreach (jeeNetwork::byPlugin('mySensors') as $jeeNetwork) {
						echo '<option value="' . $jeeNetwork->getId(). '">{{Jeedom esclave}} ' . $jeeNetwork->getName() . ' (' . $jeeNetwork->getId(). ')</option>';
					}

    echo'                <option value="network">{{Gateway Réseau}}</option>
                </select>
            </div>
        </div>


<div id="div_local" class="form-group">
            <label class="col-lg-4 control-label">{{Adresse de la Gateway}} :</label>
            <div class="col-lg-4">
                <select id="select_port" style="margin-top:5px;display:none" class="configKey form-control" data-l1key="nodeGateway">';

                    foreach (jeedom::getUsbMapping() as $name => $value) {
                        echo '<option value="' . $name . '">' . $name . ' (' . $value . ')</option>';
                    }
					echo '<option value="serie">{{Port série non listé (port manuel)}}</option>';

       echo '         </select>

				<input id="manual_port" class="configKey form-control" data-l1key="nodeAdress" style="margin-top:5px;display:none" placeholder="/dev/ttyS0 {{ou}} 192.168.1.1:5003"/>
				            </div>
        </div>

		<div id="div_inclusion" class="form-group">
		<label class="col-lg-4 control-label" >{{Inclusion}} :</label>
			<div class="col-lg-2">
			<select id="select_include" class="configKey form-control" data-l1key="include_mode">
                    	<option value="on">{{Activée}}</option>
                    	<option value="off">{{Désactivée}}</option>
                    	</select>
			</div>
		</div>

				<div class="alert alert-success">{{La sauvegarde de la configuration redémarre automatiquement le service}}</div>' ;
			}
				?>

		<script>

    $('.bt_installDeps').on('click',function(){
bootbox.confirm('{{Etes-vous sûr de vouloir réinstaller les dépendances ? Celles ci sont normalement installées à l\'activation, cette fonction ne sert que pour le debug et import de sauvegarde }}', function (result) {
  if (result) {
    $.ajax({// fonction permettant de faire de l'ajax
          type: "POST", // méthode de transmission des données au fichier php
          url: "plugins/mySensors/core/ajax/mySensors.ajax.php", // url du fichier php
          data: {
              action: "installDep",
          },
          dataType: 'json',
          global: false,
          error: function (request, status, error) {
              handleAjaxError(request, status, error);
          },
          success: function (data) { // si l'appel a bien fonctionné
              if (data.state != 'ok') {
                  $('#div_alert').showAlert({message: data.result, level: 'danger'});
                  return;
              }
          }
      });
  }
});
});

				$( "#select_port" ).change(function() {
					$( "#select_port option:selected" ).each(function() {
						if($( this ).val() == "serie"){
						 $("#manual_port").show();
						 $("#manual_port").attr("placeholder", "ex : /dev/ttyS0").blur();
						}
						else if ($("#select_mode option:selected").val() == "network"){
							$("#manual_port").show();
						}
						else {
							$("#manual_port").hide();
						}
						});
				});

				$( "#select_mode" ).change(function() {
					$( "#select_mode option:selected" ).each(function() {
						if($( this ).val() == "network"){
							$("#manual_port").show();
							$("#manual_port").attr("placeholder", "ex : 192.168.1.1:5003").blur();
							$("#select_port").hide();
						}
						else if($( this ).val() == "none"){
							$("#select_port").hide();
							$("#manual_port").hide();
						}
						else{
							$("#select_port").show();
							$("#manual_port").hide();
						    $.ajax({// fonction permettant de faire de l'ajax
						        type: "POST", // méthode de transmission des données au fichier php
						        url: "plugins/mySensors/core/ajax/mySensors.ajax.php", // url du fichier php
						        data: {
						            action: "getUSB",
						            id: $( this ).val(),
						        },
						        dataType: 'json',
						        global: false,
						        error: function (request, status, error) {
						            handleAjaxError(request, status, error);
						        },
						        success: function (data) { // si l'appel a bien fonctionné
						            if (data.state != 'ok') {
						                $('#div_alert').showAlert({message: data.result, level: 'danger'});
						                return;
						            }
									var options = '';
									for (var i in data.result) {
										options += '<option value="'+i+'">'+i+' ('+data.result[i]+')</option>';
									}
									if (options == '') {
										$("#manual_port").show();
										$("#manual_port").attr("placeholder", "ex : /dev/ttyS0").blur();
									}
									options += '<option value="serie">{{Port série non listé (port manuel)}}</option>';
									$("#select_port").html(options);
									//$("#select_port").value('serie');
									$.ajax({// fonction permettant de faire de l'ajax
								        type: "POST", // méthode de transmission des données au fichier php
								        url: "plugins/mySensors/core/ajax/mySensors.ajax.php", // url du fichier php
								        data: {
								            action: "confUSB",
								        },
								        dataType: 'json',
								        global: false,
								        error: function (request, status, error) {
								            handleAjaxError(request, status, error);
								        },
								        success: function (data) { // si l'appel a bien fonctionné
								            if (data.state != 'ok') {
								                $('#div_alert').showAlert({message: data.result, level: 'danger'});
								                return;
								            }
											$("#select_port").value(data.result);
                      modifyWithoutSave = false;
								        }
								    });
						        }
						    });
						}
					});
				});

     function mySensors_postSaveConfiguration(){
        $.ajax({// fonction permettant de faire de l'ajax
            type: "POST", // methode de transmission des données au fichier php
            url: "plugins/mySensors/core/ajax/mySensors.ajax.php", // url du fichier php
            data: {
                action: "postSave",
            },
            dataType: 'json',
            error: function (request, status, error) {
                handleAjaxError(request, status, error);
            },
            success: function (data) { // si l'appel a bien fonctionné
				if (data.state != 'ok') {
					$('#div_alert').showAlert({message: data.result, level: 'danger'});
					return;
				}
			}
		});
	}


			</script>
</div>
    </fieldset>
</form>
